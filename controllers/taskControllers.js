const Task = require("../models/task");

// Controller function for getting all task
	const taskControllers = {
		getAll: async(req, res) =>{
			try {
				const allTask = await Task.find();
				res.status(200);
				res.json(allTask);
			}catch (error){
				res.status(500);
				res.json({message: error.message})
			}
		},

		getOneTask: async (req, res) => {
			const theTask = await res.task;
			res.json(theTask);
		},

		getTaskId: async (req, res, next) =>{
			let task;
			try{
				task = await Task.findById(req.params.id);
				if(!task){
					res.status(404);
					res.json({ message: "Task not found!"});
					return;
				}
			}catch{
				res.status(500);
				res.json({ message: error});
			}
			res.task = task;
			next();
		}

	}
	module.exports = taskControllers;


// Controller function for creating a task
	module.exports.createTask = (requestBody) =>{
		let newTask = new Task({
				name: requestBody.name
		})
	
	// save created "newTask" object in the MongoDB.
		return newTask.save().then((task, error) =>{

			if(error){
				console.log(error);
				return false;

			}else{
				return task;

			}
		})
	}


// Controller function for updating a task
	module.exports.updateTask = (taskId) =>{

		return Task.findById(taskId).then((result, error) =>{ 

				if(error){
					console.log(error);
					return false;

				}else{
					result.status = "completed";

					return result.save().then((updatedTask, saveErr) =>{
						if(saveErr){
							console.log(saveErr);
							return false

						}else{
							return updatedTask;

						}
					})
				}
			}
		)
	}
