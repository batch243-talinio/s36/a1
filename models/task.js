const mongoose = require("mongoose");

// Create the Schema w/ mongoose.Schema()
	const taskSchema = new mongoose.Schema({
		status:{
			type: String,
			default: "pending..."
		},
		name:{
			type: String,
			required: true
		},
	});

module.exports = mongoose.model("Task", taskSchema);