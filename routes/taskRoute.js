const express = require("express");

const router = express.Router();

const taskControllers = require("../controllers/taskControllers")

// [Section] ROUTES

	// GET request at the URL "/tasks"
		router.get("/", taskControllers.getAll)

	// GET request oneTask
		router.get("/:id", taskControllers.getTaskId, taskControllers.getOneTask)

	// POST request at the URL "/tasks"
		router.post("/", (req, res) =>{
			taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController));
		})

	// PUT request at the URL "/tasks/:id"
		router.put("/:id/complete", (req, res) =>{

			taskControllers.updateTask(req.params.id).then(resultFromController => res.send(resultFromController))
		})

// use to export the router object to use in the app.js
module.exports = router; 